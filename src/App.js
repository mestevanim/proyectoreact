import logo from './logo.svg';
import './App.css';
import { Route, Link, Routes} from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";


function App() {
  return (
    <div className="App">
    <header>
      <Link to="/" className="Link">Home</Link>
      <Link to="/about" className="Link">About</Link>
    </header>
     <Routes>
      <Route exact path="/" element={<Home/>} />
        <Route path="/about" element={<About/>}/>
    </Routes>

    </div>
  );

}

export default App;
